﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace FM5092_8_MT
{
    public partial class Form1 : Form
    {
        int i = 0;
        delegate void updater();

        FinanceDataModelContainer db = new FinanceDataModelContainer();

        public Form1()
        {
            InitializeComponent();
        }

        private void btnDoWork_Click(object sender, EventArgs e)
        {
            progressBar1.Maximum = 5000;
            //lblStatus.Text = "DOING WORK...";
            Thread t = new Thread(new ThreadStart(Work));
            t.Start();
            //lblStatus.Text = "ALL DONE WITH WORK!";
        }

        private void Work()
        {
            int[,] abc = new int[5000, 5000];
            for(i = 0; i < 5000; i++)
            {
                this.BeginInvoke(new updater(UpdateProgress));
                for(int j = 0; j < 5000; j++)
                {
                    for(int z = 0; z < 50; z++)
                    {
                        abc[i, j] = i * j;

                    }
                }
            }
        }

        private void UpdateProgress()
        {
            progressBar1.Value = i;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            db.Customers.Add(new Customer()
            {
                First_Name = txtFirstName.Text,
                Last_Name = txtLastName.Text,
                Street = "1234 Main st",
                Phone = 1234567,
                Zip = 55120,
                State = "MN",
                EMail = txtFirstName.Text + "." + txtLastName.Text + "@gmail.com"
            });

            db.SaveChanges();
        }

        private void btnGetCustomers_Click(object sender, EventArgs e)
        {
            foreach(var c in db.Customers)
            {
                MessageBox.Show(c.First_Name + " " + c.Last_Name);
            }
        }
    }
}
